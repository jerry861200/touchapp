require('dotenv').config()
// import path from 'path'
// import fs from 'fs'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'TOUCHNOODLE｜乾拌麵購物網站｜最強熱銷品牌｜乾拌麵大賞包',
    htmlAttrs: {
      lang: 'zh-TW'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: "最齊全的乾拌麵全品牌購物網站。人氣推薦乾拌麵／名人網紅品牌／排隊名店的精選乾拌麵皆有販售。推出全台獨有乾拌麵大賞包，一次少量試到多種品牌乾拌麵。" },
      { hid: 'og:title', property: 'og:title', content: 'TOUCHNOODLE｜乾拌麵購物網站｜最強熱銷品牌｜乾拌麵大賞包' },
      { hid: 'og:description', property: 'og:description', content: "最齊全的乾拌麵全品牌購物網站。人氣推薦乾拌麵／名人網紅品牌／排隊名店的精選乾拌麵皆有販售。推出全台獨有乾拌麵大賞包，一次少量試到多種品牌乾拌麵。" },
      { hid: 'og:image', property: 'og:image', content: '/T.svg' },
      { hid: 'og:site_name', property: 'og:site_name', content: 'TOUCHNOODLE｜乾拌麵購物網站｜最強熱銷品牌｜乾拌麵大賞包' },
      { hid: 'og:locale', property: 'og:locale', content: 'zh_TW' },
      { hid: 'og:type', property: 'og:type', content: 'website' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/T.svg' },
      { rel: 'preconnect', href: "https://fonts.gstatic.com" },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500;700&display=swap" }
    ],
    script: [
      {
        hid: "TwCitySelector",
        src: "https://cdn.jsdelivr.net/npm/tw-city-selector@2.1.1/dist/tw-city-selector.min.js",
        defer: true,
      },

    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/all.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/vue-scroll',
    '~/plugins/axios',
    '~/plugins/moment',
    { src: '@/plugins/vue-awesome-swiper', mode: 'client' },
    { src: '~/plugins/vue-calendar', mode: 'client' },
    '~/plugins/vue-lazyload',
    {src:'@/plugins/gtag',mode:'client'}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    ['@nuxtjs/dotenv', { filename: '.env.' + process.env.NODE_ENV }]
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    ['nuxt-vuex-localstorage',{
      localStorage: ['LS','member']
    }],
    ['vue-scrollto/nuxt', { duration: 1500 }],
    '@nuxtjs/sitemap'
  ],
  // server: {
  //   https: {
  //     key: fs.readFileSync(path.resolve(__dirname, 'localhost-key.pem')),
  //     cert: fs.readFileSync(path.resolve(__dirname, 'localhost.pem'))
  //   }
  // },
  serverMiddleware:[
    {path:'/api',handler:'~/api/index.js'},
    {path:'/auth',handler:'~/api/auth.js'}
  ] ,
  publicRuntimeConfig: {},
  privateRuntimeConfig: {},

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    proxy:true,
  },
  proxy:{
    '/google': {
      target: 'https://script.google.com',
      pathRewrite: { "^/google": "" }
    },
  }, 

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
  sitemap: {
    hostname: 'https://touchnoodle.com',
    gzip: true,
    exclude: [
      '/last',
      '/law',
      '/buyerInfo',
    ],
    routes: [
      '/products/六包各一乾拌麵大賞包',
      '/products/曾乃對決包',
      '/products/四包各一名店組合包',
      '/products/四包各一椒麻組合包',
      '/products/千拌麵-黑麻油麵線（1袋3包）',
      '/products/大拙匠人-鵝油椒麻（1袋3包）',
      '/products/金博家 蔥蔥回魂辣拌麵(1袋4入)',
      '/products/泰麵綠咖哩 乾拌麵(湯麵)(4入)',
      '/products/【曾拌麵】香蔥椒麻(一袋4包)',
      '/products/炎選絕品-祖傳蔥油香菇4入',
      '/articles/乾拌麵大賞包',
    ]
  },
}
