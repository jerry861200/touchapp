module.exports = {
  apps : [{
    name: 'touchapp_prod',
    script: './node_modules/nuxt-start/bin/nuxt-start.js',
    instances: 'max',
    exec_mode: 'cluster',
    max_memory_restart: '1G',
    port: 4000,
  },],
};
