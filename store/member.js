import jwtDecode from 'jwt-decode'

export const state = () => ({
  isUserLoggedIn: false, //是否登入
  userName: "", //會員名稱
  userUid: "", //會員 firebase 的 uid
  userEmail: '',
  // userPicture: '',
  thirdParty: '',
  successModal: false,
})
export const mutations = {
  googleLogin: (state, payload) => {
    state.isUserLoggedIn = true;
    // state.userPicture = payload.userPicture || "https://bulma.io/images/placeholders/128x128.png";
    state.userName = payload.userName;
    state.userUid = payload.userUid;
    state.userEmail = payload.email;
    state.thirdParty = 'google';
  },
  fbLogin(state, payload) {
    state.isUserLoggedIn = true;
    state.userUid = payload.id;
    state.userEmail = payload.email;
    state.userName = payload.name;
    state.thirdParty = 'fb'
  },
  logout(state) {
    state.isUserLoggedIn = false;
    state.userName = '';
    state.userUid = '';
    state.userEmail = '';
  },
  toggleSuccessModal(state) {
    state.successModal = !state.successModal
  }
}

export const actions = {
  nuxtServerInit({ commit }, context) {
    //這邊是給 Oauth 回來時提早觸發
    if (context.query.id_token && context.query.refresh_token) {
      let id_token_Decode = jwtDecode(context.query.id_token);
      commit('setUserLoggedIn', {
        id_token: context.query.id_token,
        refresh_token: context.query.refresh_token,
        userUid: id_token_Decode.user_id,
        userPicture: id_token_Decode.picture,
        userName: id_token_Decode.name,
      });
      console.log(context)
      return
    }
  },
  showSuccessModalForABit({commit,dispatch}){
    commit('toggleSuccessModal');
    setTimeout(()=>{dispatch('closeSuccessModalForABit')},2000)
  },
  closeSuccessModalForABit({commit}){
    commit('toggleSuccessModal');
  }
}
