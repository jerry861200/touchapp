export const state = () => ({
  LS: {
    selectedProduct: [
    ],
    sender: {
      senderName: '',
      senderEmail: '',
      senderPhone: '',
    },
    receiver: {
      receiverName: '',
      receiverPhone: '',
      receiverCounty: '',
      receiverDistrict: '',
      receiverAddress: '',
    },
  },
  product: [
    {
      hot:true,
      img: require('~/assets/img/noodle/曾乃對決包/00總覽.png'),
      productName: '曾乃對決包',
      // originProductPrice: '266',
      productPrice: '266',
      slogan: '無論你是天才衝衝衝的粉絲，還是在HowHow的雙聲道之間看到乃哥說：「我的麵真的比曾拌麵好吃」，都該來吃這款「曾乃對決包」，究竟乃哥有沒有吹牛呢？辣個輸不起的男人這次能贏嗎?就由你來一探究竟!',
      metaTitle: '曾乃對決包｜輸不起辣香椒麻乾拌麵｜曾拌麵香蔥椒麻｜TOUCHNOODLE 乾拌麵購物網站',
      metaDescription: '無論你是天才衝衝衝的粉絲，還是在HowHow的雙聲道之間看到乃哥說：「我的麵真的比曾拌麵好吃」，都該來吃這款「曾乃對決包」！',
      imgs: [
        require('~/assets/img/noodle/曾乃對決包/00總覽.png'),
        require('~/assets/img/noodle/曾乃對決包/001商品總覽.png'),
        require('~/assets/img/noodle/曾乃對決包/002輸不起椒麻.png'),
        require('~/assets/img/noodle/曾乃對決包/003曾拌麵椒麻.png'),
      ],
      description: [
        '· 輸不起辣香椒麻乾拌麵Ｘ2',
        '· 曾拌麵香蔥椒麻Ｘ2',
      ],
      articles: [],
      ingredient: []
    },
    {
      hot:true,
      img: require('~/assets/img/noodle/乾拌麵大賞包首圖.png'),
      productName: '六包各一乾拌麵大賞包',
      productPrice: '429',
      // originProductPrice: '429',
      slogan: '6款人氣拌麵各1，輕鬆在家享受懶人美食。',
      metaTitle: '六包各一乾拌麵大賞包｜大拙匠人鵝油椒麻｜金博家蔥蔥回魂麵｜泰麵-綠咖哩乾/湯拌麵｜曾拌麵香蔥椒麻｜炎選絕品-祖傳蔥油香菇｜千拌麵-黑麻油麵線｜TOUCHNOODLE 乾拌麵購物網站',
      metaDescription: '6款人氣拌麵各1，輕鬆在家享受懶人美食。',
      imgs: [
        require('~/assets/img/noodle/乾拌麵大賞包首圖.png'),
        require('~/assets/img/noodle/01 大拙匠人鵝油椒麻.png'),
        require('~/assets/img/noodle/02 金博家蔥蔥回魂麵.png'),
        require('~/assets/img/noodle/03 泰麵：綠咖哩乾湯拌麵.png'),
        require('~/assets/img/noodle/04 曾拌麵-香蔥椒麻.png'),
        require('~/assets/img/noodle/05 炎選絕品伴你乾拌麵-祖傳蔥油香菇.png'),
        require('~/assets/img/noodle/06 千拌麵：黑麻油麵線.png'),
      ],
      description: [
        '· 大拙匠人鵝油椒麻 Ｘ1',
        '· 金博家蔥蔥回魂麵Ｘ1',
        '· 泰麵-綠咖哩乾/湯拌麵Ｘ1',
        '· 曾拌麵香蔥椒麻Ｘ1',
        '· 炎選絕品-祖傳蔥油香菇Ｘ1',
        '· 千拌麵-黑麻油麵線Ｘ1',
      ],
      articles: [
        {
          title: '· 6款不踩雷乾拌麵評比：輕鬆在家享受懶人美食',
          link: '/articles/乾拌麵'
        }
      ],
      ingredient: []
    },
    {
      hot:true,
      img: require('~/assets/img/noodle/名店組合包/00名店總覽.png'),
      productName: '四包各一名店組合包',
      productPrice: '315',
      // originProductPrice: '315',
      slogan: '精選四大名店各有千秋乾拌麵，在家享受米其林主廚(Baan)、排隊名店（銷魂麵舖）和人氣連鎖店（瓦城、麻膳堂）的一級享受！',
      metaTitle: '四包各一名店組合包｜瓦城泰式酸辣麵｜Baan泰式綠咖哩拌麵｜麻膳堂椒麻拌麵｜大師兄銷魂麻辣粗麵｜TOUCHNOODLE 乾拌麵購物網站',
      metaDescription: '精選四大名店各有千秋乾拌麵，在家享受米其林主廚(Baan)、排隊名店（銷魂麵舖）和人氣連鎖店（瓦城、麻膳堂）的一級享受！',
      imgs: [
        require('~/assets/img/noodle/名店組合包/00名店總覽.png'),
        require('~/assets/img/noodle/名店組合包/001瓦城泰式酸辣麵.png'),
        require('~/assets/img/noodle/名店組合包/002Baan泰式綠咖哩拌麵.png'),
        require('~/assets/img/noodle/名店組合包/003麻膳堂椒麻拌麵.png'),
        require('~/assets/img/noodle/名店組合包/004大師兄銷魂麻辣粗麵.png'),
      ],
      description: [
        '· 瓦城泰式酸辣麵Ｘ1',
        '· Baan泰式綠咖哩拌麵Ｘ1',
        '· 麻膳堂椒麻拌麵Ｘ1',
        '· 大師兄銷魂麻辣粗麵Ｘ1',
      ],
      articles: [],
      ingredient: []
    },
    {
      hot:true,
      img: require('~/assets/img/noodle/椒麻組合包/00總覽.png'),
      productName: '四包各一椒麻組合包',
      productPrice: '269',
      // originProductPrice: '269',
      slogan: '椒麻拌麵香氣濃厚尾勁帶麻，是乾拌麵的熱門選擇，也成為品牌競爭激烈的戰場。選了4款口味層次豐富的椒麻拌麵，老饕、入門都適合',
      metaTitle: '四包各一椒麻組合包｜老媽拌麵-香椿椒麻｜三風製麵波浪寬拌麵｜大拙匠人鵝油椒麻拌麵｜KiKi食品雜貨椒麻拌麵｜TOUCHNOODLE 乾拌麵購物網站',
      metaDescription: '椒麻拌麵香氣濃厚尾勁帶麻，是乾拌麵的熱門選擇，也成為品牌競爭激烈的戰場。選了4款口味層次豐富的椒麻拌麵，老饕、入門都適合',
      imgs: [
        require('~/assets/img/noodle/椒麻組合包/00總覽.png'),
        require('~/assets/img/noodle/椒麻組合包/001 Kiki椒麻.png'),
        require('~/assets/img/noodle/椒麻組合包/002老媽拌麵-香椿椒麻 .png'),
        require('~/assets/img/noodle/椒麻組合包/003三風製麵波浪寬拌麵(椒麻原味).png'),
        require('~/assets/img/noodle/椒麻組合包/004大拙匠人鵝油椒麻.png'),
      ],
      description: [
        '· KiKi食品雜貨椒麻拌麵 Ｘ1',
        '· 老媽拌麵-香椿椒麻 Ｘ1',
        '· 三風製麵波浪寬拌麵 (椒麻原味)Ｘ1',
        '· 大拙匠人鵝油椒麻拌麵Ｘ1',
      ],
      articles: [],
      ingredient: []
    },
    {
      img: require('~/assets/img/noodle/06 千拌麵：黑麻油麵線.png'),
      productName: '千拌麵-黑麻油麵線（1袋3包）',
      productPrice: '168',
      originProductPrice: '229',
      slogan: '美食達人千千暖心推薦，麻油的香氣摻和著老薑的辛辣，加上低鹽製作Q談滑順的麵線，是童年被親情包圍的味道。',
      metaTitle: '千拌麵-黑麻油麵線｜TOUCHNOODLE 乾拌麵購物網站',
      metaDescription: '美食達人千千暖心推薦，麻油的香氣摻和著老薑的辛辣，加上低鹽製作Q談滑順的麵線，是童年被親情包圍的味道。',
      imgs: [
        require('~/assets/img/noodle/06 千拌麵：黑麻油麵線.png'),
      ],
      articles: [],
      ingredient: "·麵體成份：小麥粉、水、鹽\n·醬料包成分： 薑、葵花油、純黑麻油、植物性調味料(含海鹽、葡萄糖、醬油粉、歐芹粉、百里香粉、香草粉、大豆酵素)、鹽、醬油、抗氧化劑(維生素E)\n· 麻油包成分：純黑麻油\n· 過敏原資訊：本產品含有芝麻、大豆、小麥及其製品，體質過敏者請注意標示。\n·內容物淨重：124g x 3\n·保存期限：最長一年\n·保存方式：避免陽光直射，常溫陰涼處存放，開封後請儘速食用。\n·原產地：台灣\n·其他資訊：本產品已投保 2000 萬產品責任險"
    },
    {
      img: require('~/assets/img/noodle/01 大拙匠人鵝油椒麻.png'),
      productName: '大拙匠人-鵝油椒麻（1袋3包）',
      productPrice: '198',
      originProductPrice: '225',
      slogan: '鵝油奔香x椒麻暢爽，味覺層次豐富多元，2021必須吃過的勁爆新品！\n大拙匠人x樂朋LE PONTx丸莊 強強聯手·匠心謹製，法式鵝油混搭頂級180天純釀醬油，椒麻辣味活化味蕾，蔥油香，醬油甘，辣油麻！請享受這碗充滿味覺饗宴的頂級辣麵！',
      metaTitle: '大拙匠人鵝油椒麻｜TOUCHNOODLE 乾拌麵購物網站',
      metaDescription: "鵝油奔香加上椒麻暢爽，味覺層次豐富多元，2021必須吃過的勁爆新品！由大拙匠人x樂朋LE PONTx丸莊 強強聯手，匠心謹製。法式鵝油混搭頂級180天純釀醬油，椒麻辣味活化味蕾，蔥油香，醬油甘，辣油麻！",
      imgs: [
        require('~/assets/img/noodle/01 大拙匠人鵝油椒麻.png'),
      ],
      articles: [],
      ingredient: "·麵體成份：小麥麵粉、水、鹽\n·鵝油成分： 鵝油、紅蔥頭、鹽、抗氧化劑(混合濃縮生育醇) \n· 醬油成分： 水、非基改脫脂黃豆、小麥、糖、黑豆(台灣)、食鹽、糯米、L-麩酸鈉、酵母抽出物、甜味劑(甘草萃)、葡萄柚香料\n· 辣油成分：椒麻辣油(大豆沙拉油、花椒油(花椒香料、芥花油)、辣椒油(葵花油、脂肪酸山梨醇酐酯、辣椒萃取物))、抗氧化劑(混合濃縮生育醇)\n·內容物：3包/袋\n·保存期限：最長一年\n·保存方式：避免陽光直射，常溫陰涼處存放，開封後請儘速食用。\n·原產地：台灣\n·保存期限：365日\n·廠商名稱：大拙匠人食品有限公司\n·廠商電話：02-23967851\n·廠商地址：台北市中正區新生南路一段160巷11號1樓\n·投保產品責任險字號：1406-102050058\n·食品業者登錄字號：F-200188225-00000-0"
    },
    {
      img: require('~/assets/img/noodle/02 金博家蔥蔥回魂麵.png'),
      productName: '金博家 蔥蔥回魂辣拌麵(1袋4入)',
      productPrice: '239',
      originProductPrice: '259',
      slogan: '【榮獲法國藍帶美食協會乾拌辣麵評比台灣第一！亞洲第二！】\nQ彈手摺細麵，搭配滿滿高海拔新鮮乾燥大蔥，黃金比例醬料與獨家麻辣醬配方，辣勁十足！',
      metaTitle: '金博家蔥蔥回魂辣拌麵｜TOUCHNOODLE 乾拌麵購物網站',
      metaDescription: "【榮獲法國藍帶美食協會乾拌辣麵評比台灣第一！亞洲第二！】Q彈手摺細麵，搭配滿滿高海拔新鮮乾燥大蔥，黃金比例醬料與獨家麻辣醬配方，辣勁十足！",
      imgs: [
        require('~/assets/img/noodle/02 金博家蔥蔥回魂麵.png'),
      ],
      articles: [],
      ingredient: "·麵體成份：小麥粉、水、鹽\n·醬料包成分： 水、釀造醬油 (脫脂大豆、小麥)、豆瓣醬 (基因改造黃豆、小麥)、鹽、調味劑 (L-麩酸鈉、5'-次黃嘌呤核苷磷酸二鈉、5'-鳥嘌呤核苷磷酸二鈉、琥珀酸二鈉)、調理食醋 (烏醋)、蔗糖、香辛料 (辣椒、花椒、胡椒、肉桂)、粘稠劑 (乙醯化己二酸二澱粉、玉米糖膠)、酵母抽出物、香料、異抗壞血酸鈉 (抗氧化劑)\n· 辣椒醬包：大豆油、香辛料 (紅蔥、花椒、辣椒)、棕櫚油 (完全氫化棕櫚油、大豆卵磷脂)、紅椒色素、抽出物 (辣椒、迷迭香)、維生素E (醋酸dl-α-生育醇酯) \n· 乾燥蔥包：蔥\n·內容物淨重：135g x 4 (麵100g、醬包20g、辣椒醬包10g、乾燥蔥5g)\n·保存期限：最長一年\n·廠商名稱：金博概念股份有限公司\n廠商電話：02-7701-5358\n廠商地址：台北市中山區民生東路二段163號10樓\n投保產品責任險字號：0525字第16APL0000855號\n食品業者登錄字號：A-153767559-00000-5\n·原產地：台灣\n·其他資訊：本產品已投保 2000 萬產品責任險"
    },
    {
      img: require('~/assets/img/noodle/03 泰麵：綠咖哩乾湯拌麵.png'),
      productName: '泰麵綠咖哩 乾拌麵(湯麵)(4入)',
      productPrice: '249',
      originProductPrice: '279',
      slogan: '道地泰味 「正宗泰廚手帖」\n＊酸辣鹹甜香，五味平衡，渲染舌尖\n＊採用泰國新鮮食材熬煮，無防腐劑\n＊台南日曬手工麵，質樸有韻味\n＊簡單快速料理，乾的湯的隨心所欲',
      metaTitle: '泰麵綠咖哩 乾拌麵(湯麵)｜TOUCHNOODLE 乾拌麵購物網站',
      metaDescription: '道地泰味 「正宗泰廚手帖」，酸辣鹹甜香，五味平衡，渲染舌尖。採用泰國新鮮食材熬煮，無防腐劑。台南日曬手工麵，質樸有韻味。簡單快速料理，乾的湯的隨心所欲',
      imgs: [
        require('~/assets/img/noodle/03 泰麵：綠咖哩乾湯拌麵.png'),
      ],
      articles: [],
      ingredient: "·麵體成份：小麥粉、水、鹽\n·醬包成分： 椰漿、綠咖哩辣醬（綠辣椒、泰國手指薑、香茅、南薑、孜然粉、荳蔻粉、薑黃粉、黑胡椒）、醬油（大豆抽出物、鹽、水、糖）、糖、鹽、九層塔、完全氫化棕櫚油、泰國檸檬葉、水解植物蛋白（水解大豆蛋白、 5'-次黃嘌呤核?磷酸二鈉、5'-鳥嘌呤核?磷酸二鈉）\n· 辣椒粉包：泰國辣椒\n·內容物淨重：麵體90公克＊４包、醬包55公克＊４包、辣椒粉1.5公克＊４包\n·保存期限：最長一年\n·廠商名稱：稑禎有限公司\n·廠商電話：(02)8292-2366\n·廠商地址：新北市三重區重新路五段525號10樓之1\n·投保產品責任險字號：新安東京保單號碼1722504ML000\n·食品業者登錄字號：A-123048998-00000-1"
    },
    {
      img: require('~/assets/img/noodle/04 曾拌麵-香蔥椒麻.png'),
      productName: '曾拌麵 香蔥椒麻(一袋4包)',
      productPrice: '168',
      originProductPrice: '199',
      slogan: '乾拌麵的始祖，乾拌麵新人的首選．香醇蔥油佐以辛香花椒，兼具香麻辣多重口感，和厚芯波浪刀削麵的彈勁口感搭配出絕倫滋味。',
      metaTitle: '曾拌麵香蔥椒麻｜TOUCHNOODLE 乾拌麵購物網站',
      metaDescription: "乾拌麵的始祖，乾拌麵新人的首選．香醇蔥油佐以辛香花椒，兼具香麻辣多重口感，和厚芯波浪刀削麵的彈勁口感搭配出絕倫滋味。",
      imgs: [
        require('~/assets/img/noodle/04 曾拌麵-香蔥椒麻.png'),
      ],
      articles: [],
      ingredient: "·內容物：麵、蔥油拌醬包、椒麻油包\n·內容物淨重：116公克＊４包\n·豬肉可食部位之原料原產地(國)：西班牙\n·保存期限：最長一年\n·廠商名稱：食味鮮股份有限公司\n·廠商電話：02-2693-2919\n·廠商地址：新北市汐止區福德二路215號6樓之2\n·投保產品責任險字號：0900-0333003365-03\n·食品業者登錄字號：F-128137864-00000-2"
    },
    {
      img: require('~/assets/img/noodle/05 炎選絕品伴你乾拌麵-祖傳蔥油香菇.png'),
      productName: '炎選絕品-祖傳蔥油香菇4入',
      productPrice: '249',
      originProductPrice: '279',
      slogan: '★炎家特有油蔥香菇，簡單食材豐富營養\n★濃香的蔥油醬增添了鹹香好滋味\n★蔥油以豬油提味、不含防腐劑\n★特產的香菇更代表幸福家鄉味\n★伴你乾拌麵，不只拌你的麵，也伴你的心',
      metaTitle: '炎選絕品-祖傳蔥油香菇｜TOUCHNOODLE 乾拌麵購物網站',
      metaDescription: '炎家特有油蔥香菇，簡單食材豐富營養。濃香的蔥油醬增添了鹹香好滋味。蔥油以豬油提味、不含防腐劑。特產的香菇更代表幸福家鄉味。伴你乾拌麵，不只拌你的麵，也伴你的心。',
      imgs: [
        require('~/assets/img/noodle/05 炎選絕品伴你乾拌麵-祖傳蔥油香菇.png'),
      ],
      articles: [],
      ingredient: "·麵體成份：小麥麵粉、水、食鹽、無水碳酸鈉\n· 調理包 ：油蔥香菇醬料包、釀造醬油\n·內容物淨重：98公克＊４包\n·豬肉可食部位之原料原產地(國)：西班牙\n·保存期限：最長一年\n·廠商名稱：蒸汽火車有限公司\n·廠商電話：0227316189\n·廠商地址：台北市松山區八德路二段374號11樓之8"
    },
    
  ],
  comingProduct: [
    {
      img: require('~/assets/img/Coming Soon.png'),
      productName: '大拙匠人鵝油椒麻',
      productPrice: 'coming soon',
      comingSoon: true,
    },
    {
      img: require('~/assets/img/Coming Soon.png'),
      productName: '金博家蔥蔥回魂麵',
      productPrice: 'coming soon',
      comingSoon: true,
    },
    {
      img: require('~/assets/img/Coming Soon.png'),
      productName: '曾拌麵香蔥椒麻',
      productPrice: 'coming soon',
      comingSoon: true,
    },
    {
      img: require('~/assets/img/Coming Soon.png'),
      productName: '泰麵-綠咖哩乾/湯拌麵',
      productPrice: 'coming soon',
      comingSoon: true,
    },
    {
      img: require('~/assets/img/Coming Soon.png'),
      productName: '炎選絕品-祖傳蔥油香菇',
      productPrice: 'coming soon',
      comingSoon: true,
    },
    {
      img: require('~/assets/img/Coming Soon.png'),
      productName: '千拌麵-黑麻油麵線',
      productPrice: 'coming soon',
      comingSoon: true,
    },

  ],
  recommendedProduct: [
    {
      img: require('~/assets/img/加購-可口可樂330ml2入.png'),
      productName: '可口可樂 330ml 2瓶',
      productPrice: 40,
    },
    {
      img: require('~/assets/img/加購-treetop2入.png'),
      productName: 'TreeTop蘋果汁330ml2入',
      productPrice: '50',
    },

  ],
  card: {
    showCard: false,
    to: '',
    from: '',
    msg: '',
  },
  order: {
  },
  orderTotalLength: 0,
  allOrder:{}
})

export const getters = {
  shippingFee(state, getters) {
    // let county = state.LS.receiver.receiverCounty;
    // let district = state.LS.receiver.receiverDistrict
    // if (county === '台北市') {
    //   let taipei_70 = ['信義區', '大安區', '松山區', '中正區', '中山區', '大同區', '萬華區']
    //   let include_70 = taipei_70.some((item) => {
    //     return item === district
    //   })
    //   if (include_70) { return 70 }
    //   let taipei_105 = ['內湖區', '士林區', '南港區']
    //   let include_105 = taipei_105.some((item) => {
    //     return item === district
    //   })
    //   if(include_105) {return 105}
    //   return 130
    // }
    // if(county === '新北市'){
    //   let newTaipei_105 = ['新店區','板橋區','中和區','新莊區','三重區','永和區']
    //   let include_105 = newTaipei_105.some((item) => {
    //     return item === district
    //   })
    //   if(include_105) return 105
    //   else return 130
    // }
    // return 130
    if (getters.totalProductPrice > 500) return 0;
    return 60
  },
  amountOfProduct(state) {
    if (state.LS.selectedProduct === undefined) return 0
    return state.LS.selectedProduct.length
  },
  totalProductPrice(state) {
    if (state.LS.selectedProduct.length == 0) { return 0 }
    else {
      return state.LS.selectedProduct
        .map((item) => {
          return item.productPrice * item.productQuantity;
        })
        .reduce((x, y) => {
          return x + y;
        });
    }
  },
  totalPrice(state, getters) {
    return getters.totalProductPrice + getters.shippingFee
  }
}

export const mutations = {
  addToCart(state, payload) {
    state.LS.selectedProduct.push(payload);
  },
  addProductQuantity(state, payload) {
    state.LS.selectedProduct[payload].productQuantity++;
  },
  reduceProductQuantity(state, payload) {
    state.LS.selectedProduct[payload].productQuantity--;
  },
  deleteProduct(state, payload) {
    state.LS.selectedProduct.splice(payload, 1)
  },
  addCardContent(state, payload) {
    state.card[payload.property] = payload.content
  },
  addSenderContent(state, payload) {
    state.LS.sender[payload.property] = payload.content
  },
  addReceiverContent(state, payload) {
    state.LS.receiver[payload.property] = payload.content
  },
  sameInfo(state) {
    state.LS.receiver.receiverName = state.LS.sender.senderName
    state.LS.receiver.receiverPhone = state.LS.sender.senderPhone
  },
  clearReceiverInfo(state) {
    state.LS.receiver.receiverName = ''
    state.LS.receiver.receiverPhone = ''
  },
  showCard(state, payload) {
    state.card.showCard = payload
  },
  createOrder(state, payload) {
    state.order = payload
  },
  orderTotalLength(state, payload) {
    state.orderTotalLength = payload
  },
  clearnSelectedProduct(state) {
    state.LS.selectedProduct = []
    state.LS.sender.senderName = ''
    state.LS.sender.senderEmail = ''
    state.LS.sender.senderPhone = ''
    state.LS.receiver.receiverName = ''
    state.LS.receiver.receiverPhone = ''
    state.LS.receiver.receiverCounty = ''
    state.LS.receiver.receiverDistrict = ''
    state.LS.receiver.receiverAddress = ''
  },
  changeDate(state, payload) {
    let { index, start, end } = payload
    state.LS.selectedProduct[index].start = start
    state.LS.selectedProduct[index].end = end
  },
  getAllOrder(state,payload){
    state.allOrder = payload
  }
}

export const actions = {
  async OrderLengthFromDB(context) {
    let data = await this.$axios({
      method: "get",
      url:
        `${process.env.firebase_url}/order.json`,
    });
    context.commit('getAllOrder',data.data)
    let orderTotalLength = Object.keys(data.data).length;
    context.commit('orderTotalLength', orderTotalLength)
  }
}