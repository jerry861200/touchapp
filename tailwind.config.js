module.exports = {
  mode:'jit',
  purge: [
    './components/**/*.{vue,js}',
     './layouts/**/*.vue',
     './pages/**/*.vue',
     './plugins/**/*.{js,ts}',
     './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      width:{
        '960': '960px',
        'lg': '1024px'
      },
      height:{
        '250':'250px',
        '66': '66px'
      },
      colors:{
        primary:'#D06C4C',
        secondary: '#E8D9CE',
        yel:'#FFBB05',
        gra:'#F7F7F7',
        dark:'#555555',
        warn:'#A01A00',
        orange:'#cc543a',
        'warm-gray':'#F7F4F0',
        p:'#666666',
        title:'#333333'
      }
      
    },
  },
  colors:{
    green:{
      DEFAULT: '#497146'
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
