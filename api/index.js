const express = require('express');
const app = express();
const https = require('https');

let env = require('dotenv').config({path: ".env." + process.env.NODE_ENV}).parsed

app.use(express.json())
app.use(express.urlencoded({
  extended: false
}))


//Tappay.js
app.post('/pay-by-prime', (req, res, next) => {
  let tappay_api_url = process.env.NODE_ENV == 'development' ? 'sandbox.tappaysdk.com' : 'prod.tappaysdk.com'
  
  const post_data = {
    "prime": req.body.prime,
    "partner_key": "partner_E2J06tzvhysPz3XwwMBEPo0luMerCAXgkhh7q3FV25R7CzNyOInT9cnO",
    "merchant_id": "Touchbox_ESUN",
    "amount": req.body.amount,
    "currency": "TWD",
    "details": req.body.details,
    "cardholder": req.body.cardholder,

    "remember": false
  }

  const post_options = {
    host: tappay_api_url,
    port: 443,
    path: '/tpc/payment/pay-by-prime',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': 'partner_E2J06tzvhysPz3XwwMBEPo0luMerCAXgkhh7q3FV25R7CzNyOInT9cnO'
    }
  }

  const post_req = https.request(post_options, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (body) {
      return res.json({
        result: JSON.parse(body)
      })
    });
  });
  req.on('error', (e) => {
    console.error(e);
  });

  post_req.write(JSON.stringify(post_data));
  post_req.end();

})
//綠界
app.post('/ReceiverServerReply',(req,res)=>{
  const data = req.body.CVSStoreName
  res.redirect(`${env.web_url}/buyerInfo?seven=${data}`)
})


module.exports = app;