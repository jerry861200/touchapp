import Vue from 'vue';
import VueGtag from 'vue-gtag';

export default ({ app }) => {
  Vue.use(VueGtag, {
    config: {
      id: 'G-W415Z8WR6W',
      debug:false,
      params:{
        send_page_view: true
      }
    },
    debug: false,
  }, app.router);
}
